import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getMatchInfo } from "../../../actions/balance";

const BetDetails = React.createClass({

    propTypes: {
        id: PropTypes.string,
        dataSelectorKey: PropTypes.string,
        router: PropTypes.object.isRequired,
        swarmData: PropTypes.object.isRequired,
        previousPath: PropTypes.string
    },
    getInitialState: function () {
        return { counter: 0, };
    },
    shouldComponentUpdate: function (nextProps, nextState) {
        if (this.state == null)
            return true;
        if (this.state.counter == nextState.counter)
            return false;

        return true;
    },
    /**
     * @name getSelectedBet
     * @description get selected bet
     * @returns {undefined}
     * */

    getSelectedBet() {
        let key = this.props.previousPath && this.props.previousPath.indexOf("open-bets") !== -1 ? "betHistory_unsettled" : "betHistory_settled",
            data = this.props.swarmData.data[key],
            bets = data && data.bets; //eslint-disable-line react/prop-types

        if (bets) {
            this.selectedBet = bets.find(bet => (bet.id === this.props.id ? bet : undefined));
            if (this.selectedBet && this.selectedBet.events.length > 0) {
                let events = this.selectedBet.events;
                let counters = 0;
                this.selectedBet.events.forEach(async (d) => {
                    counters = counters + 1;
                    let newMatchInfo = await getMatchInfo(d)
                    d.newMatchInfo = newMatchInfo
                    if (counters === this.selectedBet.events.length) {
                        const timer = setTimeout(() => {
                            this.setState({ counter: 1 });
                        }, 200);
                    }
                });


            }
            // console.log('*&*&*',this.selectedBet)
        }
    },
    componentWillMount() {
        this.selectedBet = null;
        this.getSelectedBet();
        if (!this.selectedBet) {     //eslint-disable-line react/prop-types
            this.context.router.push("/history/bets");
        } else {
            //console.log("bet found in store", this.selectedBet);
        }
    },
    componentWillReceiveProps(nextProps) {
        if (!this.selectedBet && nextProps.swarmData.data[this.props.dataSelectorKey]) { //eslint-disable-line react/prop-types
            this.getSelectedBet();
        }
    },
    render() {
        return Template.apply(this); //eslint-disable-line no-undef
    }
});

BetDetails.contextTypes = {
    router: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        preferences: state.preferences,
        swarmData: state.swarmData,
        previousPath: state.uiState.previousPath
    };
}

export default connect(mapStateToProps)(BetDetails);
