import Config from "../../config/main";
import {framesCount} from "../../helpers/sport/gameInfo";
import { t } from "../../helpers/translator";
const replacements = {
        "eng": {
            "Team 1": "team1_name",
            "Player 1": "team1_name",
            "team 1": "team1_name",
            "Team 2": "team2_name",
            "Player 2": "team2_name",
            "team 2": "team2_name",
            "W1": "team1_name",
            "W2": "team2_name",
            "Home": "team1_name",
            "Away": "team2_name"
        },
        "rus": {
            "Ком. 1": "team1_name",
            "Ком1": "team1_name",
            "Ком. 2": "team2_name",
            "Ком2": "team2_name",
            "П1": "team1_name",
            "П2": "team2_name",
            "W1": "team1_name",
            "W2": "team2_name",
            "Home": "team1_name",
            "Away": "team2_name"
        },
        "arm": {
            "Խաղացող 1": "team1_name",
            "Խաղացող 2": "team2_name",
            "Թիմ 1": "team1_name",
            "Թիմ 2": "team2_name",
            "Հ1": "team1_name",
            "Հ2": "team2_name"
        },
        "tur": {
            "G1": "team1_name",
            "G2": "team2_name",
            "Home": "team1_name",
            "Away": "team2_name"
        }
    },
    exactReplacements = {
//            '1' : 'team1_name',
        ' 1': 'team1_name',
        '1 ': 'team1_name',
//            '2' : 'team2_name',
        '2 ': 'team2_name',
        ' 2': 'team2_name'
    },
    cache = {};

/**
 * Returns a readable event name (replaces special values like "1", "W1", "team1", etc. with real team name)
 * @param {String} rawName raw event name
 * @param {Object} game the game object to take team names from
 * @returns {*}
 */
export function niceEventName (rawName, game) {
    if (!rawName) {
        return;
    }
    var cacheKey = rawName + (game && (game.id || ''));
    if (cache[cacheKey] === undefined) {
        cache[cacheKey] = rawName;
        var lang = replacements[Config.env.lang] === undefined ? 'eng' : Config.env.lang;
        if (exactReplacements[cache[cacheKey]]) {
            cache[cacheKey] = game[exactReplacements[cache[cacheKey]]];
        } else if (replacements[lang]) {
            Object.keys(replacements[lang]).map((term) => {
                let fieldName = replacements[lang][term];
                if (game && game[fieldName]) {
                    while ((game[fieldName].lastIndexOf(term) === -1) && (cache[cacheKey] != cache[cacheKey].replace(term, game[fieldName] + ' '))) {
                        cache[cacheKey] = cache[cacheKey].replace(term, game[fieldName] + ' ');
                    }
                }
            });
        }
    }
    return cache[cacheKey];
}

export function createAdditionalTextInfo (game) {
    
    var whitSpace = ' ';
        var isRtl = Config.main.availableLanguages[Config.env.lang].rtl;
        var comma = isRtl ? '،' : ',';
        var firstIndex = isRtl ? 2 : 1;
        var secondIndex = firstIndex === 1 ? 2 : 1;
        var star = "*";

        var result = [];

        var scoresArray = framesCount(game.stats);
        if (game && game.info && game.info.score1 && game.info.score2) {
            result.push(game.info['score' + firstIndex] + whitSpace + ":" + whitSpace + game.info['score' + secondIndex]);
        }
        var scoresLength = scoresArray.length;
        var stats = game.stats;
        for (var i = 0; i < scoresLength; ++i) {
            if (result.length > 0) {
                result.push(comma, whitSpace);
            }
            var key = 'score_set' + scoresArray[i];
            var score = stats[key]['team$1_value'.replace('$1', firstIndex)] + ':' + stats[key][
                ['team$1_value'.replace('$1', secondIndex)]
            ];
            result.push('(' + score + ')');
        }
        if (game && game.info && game.info.current_game_state && game.info.current_game_state === 'notstarted' && game.info.current_game_time == 0) {
            var notStarted = t('Not Started');
            result = [isRtl ? notStarted.split("").reverse().join("") : notStarted];
        } else if (game && game.info && game.info.current_game_time) {
            result.push(whitSpace);
            result.push(game.info.current_game_time);
            if (game.info.current_game_time.indexOf(':') === -1) {
                result.push('`');
            }
        }
        if (game && game.Sport && (game.Sport.alias === 'Tennis' || game.Sport.alias === 'ETennis')) {
            result.push(whitSpace);
            var passes = stats.passes;
            var passTeamIndex = game.info.pass_team === 'team1' ? 1 : 2;
            result.push(passes.team1_value);
            if (passTeamIndex === 1) {
                result.push(star)
            }
            result.push(":");
            result.push(passes.team2_value);
            if (passTeamIndex === 2) {
                result.push(star)
            }
        }

        if (isRtl) {
            return result.reverse().join('');
        }

        return result.join('');
}



/**
 * Displays event base based on market type and base value
 * @param {Object} event
 * @param initialBase
 * @returns {string}
 */
export function displayEventBase (event, initialBase = null) {
    var baseFieldName = initialBase ? 'baseInitial' : 'base';
    var prefix = (event.marketType && event.marketType.substr(-8) === 'Handicap' && event[baseFieldName] > 0 ? '+' : '');

    return prefix + (event.eventBases && !Config.main.displayEventsMainBase ? event.eventBases.join("-") : event[baseFieldName]);
}
