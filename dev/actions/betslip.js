import Zergling from '../helpers/zergling';
import Config from "../config/main";
import {
    BETSLIP_ADD, BETSLIP_REMOVE, BETSLIP_CLEAR, BETSLIP_SET_TYPE, BETSLIP_TOGGLE_QUICKBET, BETSLIP_ACCEPT_CHANGES,
    BETSLIP_BET_ACCEPTED, BETSLIP_BET_FAILED, BETSLIP_SET_STAKE, BETSLIP_SET_UNIT_STAKE, BETSLIP_SET_EACH_WAY_MODE,
    BETSLIP_SET_SYSTEM_OPT, BETSLIP_SET_ACCEPT_OPT, BETSLIP_SET_INCLUDE_IN_SYSTEM_CALC, BETSLIP_RESET_STATUS,
    BETSLIP_BET_PROCESSING, BETSLIP_QUICK_BET_NOTIFY, BETSLIP_QUICK_BET_NOTIFY_DISMISS, BETSLIP_TOGGLE_FREEBET,
    BETSLIP_TOGGLE_SUPERBET, BETSLIP_SET_FREE_BETS, BETSLIP_SELECT_FREE_BET, BETSLIP_TOGGLE_FREEBET_LOADING_STATE,
    BETSLIP_TOGGLE_BOOKINGBET
} from './actionTypes/';
import { createAdditionalTextInfo } from "../helpers/sport/eventNames";

import Helpers from "../helpers/helperFunctions";
import { t } from "../helpers/translator";
import { getErrorMessageByCode, errorCodes } from "../constants/errorCodes";

import { BETSLIP_TYPE_SYSTEM, createBetRequests } from "../helpers/sport/betslip";

import { UILoading, UILoadingDone, UILoadingFailed, OpenPopup, ClosePopup, UIClose } from "./ui";
import { FavoriteAdd } from "./favorites";
import { GetUserBalance } from "./balance";
import cookie from 'react-cookie';
import moment from "moment";
import _ from "lodash";

import {SwarmReceiveData} from "../actions/swarm";



const axios = require('axios');

/**
 * @name BetslipAdd
 * @description Add
 * @param {Object} payload bet event info
 * @returns {Function} async action dispatcher
 */
export const BetslipAdd = (payload) => {
    /**
     * @event betslipAdd
     */
    return {
        type: BETSLIP_ADD,
        payload
    };
};

/**
 * @name BetslipRemove
 * @description Remove given event from betslip
 * @param {Number} eventId
 * @returns {Object} New state
 */
export const BetslipRemove = (eventId) => {
    /**
     * @event betslipRemove
     */
    return {
        type: BETSLIP_REMOVE,
        payload: { eventId }
    };
};

/**
 * @name BetslipClear
 * @description Clear all events from betslip
 * @param {Boolean} processedOnly
 * @returns {Object} New state
 */
export const BetslipClear = (processedOnly = false) => {
    /**
     * @event betslipClear
     */
    return {
        type: BETSLIP_CLEAR,
        processedOnly
    };
};

/**
 * @name BetslipMarkBetProcessing
 * @description Mark in progress Bets
 * @param {Array} eventIds
 * @param {Boolean} state
 * @returns {Object} New state
 */
export const BetslipMarkBetProcessing = (eventIds, state = true) => {
    /**
     * @event betslipMarkBetProcessing
     */
    return {
        type: BETSLIP_BET_PROCESSING,
        eventIds,
        state
    };
};
/**
 * @name BetslipSetType
 * @description Set betslip type
 * @param {Number} betSlipType
 * @returns {Object} New state
 */
export const BetslipSetType = (betSlipType) => {
    /**
     * @event betslipSetType
     */
    // debugger;
    cookie.save('BETSLIP_TYPE', betSlipType);
    cookie.save('maxBetMsg', '');
    return {
        type: BETSLIP_SET_TYPE,
        betSlipType
    };
};

/**
 * @name BetslipSetFreeBets
 * @description Set free bets
 * @param {Object} payload
 * @returns {Object} New state
 */
export const BetslipSetFreeBets = (payload) => {
    /**
     * @event betslipSetFreeBets
     */
    return {
        type: BETSLIP_SET_FREE_BETS,
        payload
    };
};

/**
 * @name BetslipSelectFreeBet
 * @description Select free Bet Events
 * @param {Number} id
 * @param {Number} amount
 * @returns {Object} New state
 */
export const BetslipSelectFreeBet = (id, amount) => {
    /**
     * @event betslipSelectFreeBet
     */
    return {
        type: BETSLIP_SELECT_FREE_BET,
        id,
        amount
    };
};
/**
 * @name BetslipToggleQuickBet
 * @description  Toggle quick bet switcher
 * @param {Boolean} enabled
 * @returns {Object} New state
 */
export const BetslipToggleQuickBet = (enabled) => {
    /**
     * @event betslipToggleQuickBet
     */
    return {
        type: BETSLIP_TOGGLE_QUICKBET,
        enabled
    };
};

/**
 * @name BetslipToggleBookingBet
 * @description  Toggle booking bet switcher
 * @param {Boolean} enabled
 * @returns {Object} New state
 */
export const BetslipToggleBookingBet = (enabled) => {
    /**
     * @event betslipToggleBookingBet
     */
    return {
        type: BETSLIP_TOGGLE_BOOKINGBET,
        enabled
    };
};

/**
 * @name BetslipToggleFreeBet
 * @description  Toggle free bet switcher
 * @param {Boolean} enabled
 * @returns {Object} New state
 */
export const BetslipToggleFreeBet = (enabled) => {
    /**
     * @event betslipToggleFreeBet
     */
    return {
        type: BETSLIP_TOGGLE_FREEBET,
        enabled
    };
};

/**
 * @name BetslipToggleSuperBet
 * @description  Toggle super bet switcher
 * @param {Boolean} enabled
 * @returns {Object} New state
 */
export const BetslipToggleSuperBet = (enabled) => {
    /**
     * @event betslipToggleSuperBet
     */
    return {
        type: BETSLIP_TOGGLE_SUPERBET,
        enabled
    };
};

/**
 * @name BetslipAcceptChanges
 * @description Accept Odds change
 * @returns {Object} New state
 */
export const BetslipAcceptChanges = () => {
    /**
     * @event betslipAcceptChanges
     */
    return {
        type: BETSLIP_ACCEPT_CHANGES
    };
};

/**
 * @name BetslipBetAccepted
 * @description  Bet accepted by backend
 * @param {Object} payload
 * @returns {Object} New state
 */
export const BetslipBetAccepted = (payload) => {
    /**
     * @event betslipBetAccepted
     */
    return {
        type: BETSLIP_BET_ACCEPTED,
        payload
    };
};

/**
 * @name BetslipBetFailed
 * @description  Bet not accepted by backend
 * @param {Object} payload
 * @returns {Object} New state
 */
export const BetslipBetFailed = (payload) => {
    //console.log('*******************+++++++++++',payload)
    /**
     * @event betslipBetFailed
     */
    return {
        type: BETSLIP_BET_FAILED,
        payload
    };
};

/**
 * @name BetslipQuickBetNotify
 * @description Show Quick bet result
 * @param {Number} id
 * @param {String} resultType
 * @param {String} payload
 * @returns {Object} New state
 */
export const BetslipQuickBetNotify = (id, resultType, payload) => {
    /**
     * @event betslipQuickBetNotify
     */
    return {
        type: BETSLIP_QUICK_BET_NOTIFY,
        id,
        resultType,
        payload
    };
};

/**
 * @name BetslipQuickBetDismissNotification
 * @description Dismiss Quick bet Notification for given bet id
 * @param {Number} id
 * @returns {Object} New state
 */
export const BetslipQuickBetDismissNotification = (id) => {
    /**
     * @event betslipQuickBetDismissNotification
     */
    return {
        type: BETSLIP_QUICK_BET_NOTIFY_DISMISS,
        id
    };
};

/**
 * @name BetslipResetStatus
 * @description Reset betslip status
 * @returns {Object} New state
 */
export const BetslipResetStatus = () => {
    /**
     * @event betslipResetStatus
     */
    return {
        type: BETSLIP_RESET_STATUS
    };
};
/**
 * @name BetslipSetStake
 * @description Set betslip Stake
 * @param {Number} value
 * @param {Number} eventId
 * @returns {Object} New state
 */
export const BetslipSetStake = (value, eventId) => {
    /**
     * betslipSetStake
     */
    return {
        type: BETSLIP_SET_STAKE,
        value,
        eventId
    };
};

/**
 * @name BetslipSetInclInSysCalc
 * @description Set betslip Stake
 * @param {Number} value
 * @param {Number} eventId
 * @returns {Object} New state
 */
export const BetslipSetInclInSysCalc = (value, eventId) => {
    /**
     * @event betslipSetInclInSysCalc
     */
    return {
        type: BETSLIP_SET_INCLUDE_IN_SYSTEM_CALC,
        value,
        eventId
    };
};
/**
 * @name BetslipSetUnitStake
 * @description Set betslip unit stake
 * @param {Number} value
 * @param {Number} eventId
 * @returns {Object} New state
 */
export const BetslipSetUnitStake = (value, eventId) => {
    /**
     * @event betslipSetUnitStake
     */
    return {
        type: BETSLIP_SET_UNIT_STAKE,
        value,
        eventId
    };
};


/**
 * @name BetslipSetEachWayMode
 * @description  Enable or disable each way mode
 * @param {Boolean} value
 * @returns {Object} New state
 */
export const BetslipSetEachWayMode = (value) => {
    /**
     * @event betslipSetEachWayMode
     */
    return {
        type: BETSLIP_SET_EACH_WAY_MODE,
        value
    };
};

/**
 * @name BetslipSetSystemOpt
 * @description Set betslip system Option
 * @param {Number} value option type
 * @returns {Object} New state
 */
export const BetslipSetSystemOpt = (value) => {
    /**
     * @event BetslipSetSystemOpt
     */
    return {
        type: BETSLIP_SET_SYSTEM_OPT,
        value
    };
};

/**
 * @name BetslipSetAcceptOpt
 * @description Set betslip odds change accepting options
 * @param {Number} value
 * @returns {Object} New state
 */
export const BetslipSetAcceptOpt = (value) => {
    /**
     * @event betslipSetAcceptOpt
     */
    return {
        type: BETSLIP_SET_ACCEPT_OPT,
        value
    };
};

/**
 * @name BetslipToggleFreeBetLoadingState
 * @description toggle free bet loading state
 * @param {bool} payload
 * @returns {object} plain JavaScript object
 */
export const BetslipToggleFreeBetLoadingState = (payload) => {
    /**
     * @event betslipToggleFreeBetLoadingState
     */
    return {
        type: BETSLIP_TOGGLE_FREEBET_LOADING_STATE,
        payload
    };
};

/**
 * @name BetslipGetEventMaxBet
 * @description Get max bet value for given event ids
 * @param {Object | Array} betEvents
 * @returns {Function} async action dispatcher
 */
export const BetslipGetEventMaxBet = (betEvents) => {
    /**
     * @event betslipGetEventMaxBet
     */

    //let betEvents=betEvents;


    return function (dispatch) {

        dispatch(UILoading("getMaxBet"));
        cookie.save('maxBetMsg', '');

        let request = {}, id;
        if (Array.isArray(betEvents)) {
            request.events = betEvents.map(betEvent => betEvent.eventId);
        } else {
            request.events = [betEvents.eventId];
            id = betEvents.eventId;
        }


        Zergling.get(request, 'get_max_bet')
            .then(result => {
                //debugger;
                dispatch(BetslipSetStake(parseFloat(result.result), id));
                dispatch(BetslipSetUnitStake(parseFloat(result.result / 2), id));
                dispatch(UILoadingDone("getMaxBet"));

                // cookie.save('game_total_weight', "");

                console.log(result.result);

                cookie.save('final_max_bet_amount_keyboard', result.result);

                // if (Array.isArray(betEvents)) {
                //     cookie.save('final_maxbet_amount', result.result);
                // }else{

                //     // let max_bet_amount_json = cookie.load('max_bet_amount_json') || [];
                //     // let selectBetObj = {};

                //     let max_bet_amount_json = JSON.parse(cookie.load('max_bet_amount_json'));

                //     let foundObj = false;

                //     if(typeof max_bet_amount_json != "undefined"){

                //         for(var i=0;i<max_bet_amount_json.length;i++){
                //             if(max_bet_amount_json[i].event_id == betEvents.eventId){
                //                 foundObj=true;
                //                 max_bet_amount_json[i].event_id = betEvents.eventId;
                //                 max_bet_amount_json[i].max_bet_amount = result.result;
                //             }
                //         }
                //     }
                //     else{
                //         foundObj=true;
                //     }

                //     if(foundObj){
                //         max_bet_amount_json.push({
                //             "event_id":betEvents.eventId,
                //             "max_bet_amount":result.result
                //         })
                //     }

                //     cookie.save('max_bet_amount_json', JSON.stringify(max_bet_amount_json));

                //     console.log(result.result);

                //     cookie.save('maxBetMsg','');



                //     // for(var i=0;i<max_bet_amount_json.length;i++){
                //     //     if(max_bet_amount_json[i].event_id == betEvents.eventId){
                //     //         selectBetObj=max_bet_amount_json[i];
                //     //     }
                //     // }

                //     // var final_maxbet_amount = parseFloat(result.result);

                //     // if (parseFloat(betEvents.singleStake) > final_maxbet_amount) {

                //     //     let lastMaxBetResult = final_maxbet_amount;
                //     //     cookie.save('lastMaxBetResult', lastMaxBetResult);
                //     //     //if(!keypressEvent) betEvents.singleStake = final_maxbet_amount;

                //     //     dispatch(BetslipSetStake(final_maxbet_amount, betEvents.eventId));
                //     //     dispatch(BetslipSetUnitStake(final_maxbet_amount, betEvents.eventId));



                //     // }
                //     // else {
                //     //     dispatch(BetslipSetStake(final_maxbet_amount, betEvents.eventId));
                //     //     dispatch(BetslipSetUnitStake(final_maxbet_amount, betEvents.eventId));
                //     // }
                // }









            })
            .catch((reason) => {
                dispatch(UILoadingFailed("getMaxBet", reason));
            });


        // let request = {}, id;
        // if (Array.isArray(betEvents)) {
        //     request.events = betEvents.map(betEvent => betEvent.eventId);
        // } else {
        //     request.events = [betEvents.eventId];
        //     id = betEvents.eventId;
        // }

        // console.log(request);

        // Zergling.get(request, 'get_max_bet')
        //     .then(result => {
        //         dispatch(BetslipSetStake(parseFloat(result.result), id));
        //         dispatch(BetslipSetUnitStake(parseFloat(result.result / 2), id));
        //         dispatch(UILoadingDone("getMaxBet"));

        //         // cookie.save('game_total_weight', "");
        //         // cookie.save('final_maxbet_amount', result.result);

        //         let max_bet_amount_json = JSON.parse(cookie.load('max_bet_amount_json'));

        //         let foundObj = false;

        //         if(typeof max_bet_amount_json != "undefined"){

        //             for(var i=0;i<max_bet_amount_json.length;i++){
        //                 if(max_bet_amount_json[i].event_id == betEvents.eventId){
        //                     foundObj=true;
        //                     max_bet_amount_json[i].event_id = betEvents.eventId;
        //                     max_bet_amount_json[i].max_bet_amount = result.result;
        //                 }
        //             }
        //         }
        //         else{
        //             foundObj=true;
        //         }

        //         if(foundObj){
        //             max_bet_amount_json.push({
        //                 "event_id":betEvents.eventId,
        //                 "max_bet_amount":result.result
        //             })
        //         }

        //         cookie.save('max_bet_amount_json', JSON.stringify(max_bet_amount_json));

        //         console.log(result.result);


        //     })
        //     .catch((reason) => {
        //         dispatch(UILoadingFailed("getMaxBet", reason));
        //     });
    };
};

/**
 * @name GetSuperBetInfo
 * @description Get super bet info by bet ib
 * @param {Number} betId bet id
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const GetSuperBetInfo = (betId) => {
    /**
     * @event getSuperBetInfo
     */
    return function (dispatch) {
        dispatch(UILoading("getSuperBetInfo"));
        Zergling.get({ where: { bet_id: betId, outcome: 0 } }, 'bet_history')
            .then(result => {
                //console.log("Super bet info recived:", result);
                dispatch(UILoadingDone("getSuperBetInfo"));
                dispatch(OpenPopup("CounterOfferDialog", { betInfo: result.bets[0] }));
            })
            .catch((reason) => {
                dispatch(UILoadingFailed("getSuperBetInfo", reason));
            });
    };
};

/**
 * @name AcceptCounterOffer
 * @description Accept super bet counter offer
 * @param {number} betId bet id
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const AcceptCounterOffer = (betId) => {
    /**
     * @event acceptCounterOffer
     */
    return function (dispatch) {
        dispatch(UILoading("acceptCounterOffer"));
        Zergling.get({ bet_id: betId, accept: true }, 'super_bet_answer')
            .then(result => {
                // TODO: process result
                //console.log("approve_super_bet result:", result);
                if (result.result === 0) {
                    dispatch(UILoadingDone("acceptCounterOffer"));
                    dispatch(ClosePopup("CounterOfferDialog"));
                    dispatch(OpenPopup("message", { title: t("Counter Offer"), type: "info", body: t("Offer accepted") }));
                } else {
                    let OfferErrorMessage = errorCodes[result.result] ? t(getErrorMessageByCode(result.result)) : (t("Sorry we have some problems") + " (" + result.result + ")");
                    dispatch(OpenPopup("message", { title: t("Counter Offer"), type: "info", body: OfferErrorMessage }));
                }
            })
            .catch((reason) => {
                dispatch(UILoadingFailed("acceptCounterOffer", reason));
            });
    };
};

/**
 * @name DeclineCounterOffer
 * @description Decline super bet counter offer
 * @param {Number} betId bet id
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const DeclineCounterOffer = (betId) => {
    /**
     * @event declineCounterOffer
     */
    return function (dispatch) {
        dispatch(UILoading("declineCounterOffer"));
        Zergling.get({ bet_id: betId, accept: false }, 'super_bet_answer')
            .then(result => {
                // TODO: process result
                //console.log("decline_super_bet result:", result);
                if (result.result === 0) {
                    dispatch(UILoadingDone("declineCounterOffer"));
                    dispatch(ClosePopup("CounterOfferDialog"));
                    dispatch(OpenPopup("message", { title: t("Counter Offer"), type: "info", body: t("Offer declined") }));
                } else {
                    let OfferErrorMessage = errorCodes[result.result] ? t(getErrorMessageByCode(result.result)) : (t("Sorry we have some problems.") + " (" + result.result + ")");
                    dispatch(OpenPopup("message", { title: t("Counter Offer"), type: "info", body: OfferErrorMessage }));
                }
            })
            .catch((reason) => {
                dispatch(UILoadingFailed("declineCounterOffer", reason));
            });
    };
};

/**
 * @name BetslipLoadFreeBets
 * @description Loads free bet options for bets in betslip
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const BetslipLoadFreeBets = () => {
    /**
     * @event betslipLoadFreeBets
     */
    return function (dispatch, getState) {
        let requests = createBetRequests(getState().betslip, null, true);
        if (requests && requests.length) {
            dispatch(BetslipToggleFreeBetLoadingState(true));
            Zergling.get(requests[0], 'get_freebets_for_betslip')
                .then(result => {
                    if (result && result.details && result.details.length) {
                        dispatch(BetslipSetFreeBets(result.details.reduce((acc, curr) => {
                            curr.amount && acc.push(curr);
                            return acc;
                        }, [])));
                    } else {
                        dispatch(BetslipSetFreeBets([]));
                        dispatch(BetslipSelectFreeBet(null));
                    }
                    dispatch(BetslipToggleFreeBetLoadingState(false));
                })
                .catch((reason) => {
                    console.warn("get_freebets_for_betslip error", requests, reason);
                    dispatch(BetslipSetFreeBets([]));
                    dispatch(BetslipSelectFreeBet(null));
                    dispatch(BetslipToggleFreeBetLoadingState(false));
                });
        }
    };
};

/**
 * @description Search sport events
 * @param {Object} results
 * @returns {Object} Search results
 */
function processSearchResults(results) {
    let searchResults = [], order = 0;
    results.map(
        result => {
            Object.keys(result.data.sport).map(
                sportId => Object.keys(result.data.sport[sportId].region).map(
                    regionId => Object.keys(result.data.sport[sportId].region[regionId].competition).map(
                        competitionId => Object.keys(result.data.sport[sportId].region[regionId].competition[competitionId].game).map(
                            gameId => {
                                let sport = result.data.sport[sportId] ? result.data.sport[sportId] : {},
                                    region = sport.region[regionId] ? sport.region[regionId] : {},
                                    competition = region.competition[competitionId] ? region.competition[competitionId] : {},
                                    game = competition.game[gameId] ? competition.game[gameId] : {};
                                // searchResults[sport.id] = searchResults[sport.id] || { order: order++, results: [], name: sport.name, alias: sport.alias };
                                // searchResults[sport.id].results.push({
                                //     game: game,
                                //     start_ts: game.start_ts, //for sorting
                                //     region: { alias: region.alias },
                                //     competition: { name: competition.name, id: competition.id }
                                // });
                                // let newObj = {
                                //     "sport": sport
                                // }
                                // newObj.sport['region'] = region;
                                // newObj.sport['region']['competition'] = competition;
                                // newObj.sport['region']['competition']['game'] = game;

                                let objData = {};
                                objData.sport = region ? sport : {};
                                objData.region = region ? region : {};
                                objData.competition = competition ? competition : {};
                                objData.game = game ? game : {};

                                searchResults.push(objData)
                            }
                        )
                    )
                )
            )
        }
    );
    return searchResults;
}


/**
 * @name BetslipPlaceBet
 * @description Places a bet
 * @param {Object} betSlipData betslip data from store
 * @param {Object} currency currency object, needed for calculating superbet limits
 * @param {bool} freeBet
 * @param {Object} profile
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const BetslipPlaceBet = (betSlipData, currency = null, freeBet = false, profile) => {

    /**
     * @name getBetSlipError
     * @description generating betslip error
     * @param {object} result
     * @returns {string}
     */
    function getBetSlipError(result) {
        var code = Math.abs(parseInt(result.code || result.result || 99, 10)).toString();
        var isKnownError = getErrorMessageByCode(code, false);
        return isKnownError ? t(getErrorMessageByCode(code)) : (t("Sorry, we can't accept your bets now, please try later.") + " " + code);
    }

    /**
     * @name showQuickBetNotification
     * @description generating notification for quick bets
     * @param {object} responseData
     * @param {string} type
     * @param {function} dispatch
     * @returns {string}
     * @fire event:showQuickBetNotification
     */
    function showQuickBetNotification(responseData, type, dispatch) {
        if (betSlipData.quickBet) {
            let eventId = Helpers.firstElement(betSlipData.events).eventId;
            dispatch(BetslipQuickBetNotify(eventId, type, type === "error" ? getBetSlipError(responseData) : t("Bet {1} accepted", eventId)));
            setTimeout(() => { dispatch(BetslipQuickBetDismissNotification(eventId)); }, Config.betting.quickBetNotificationsTimeout);
        }
    }


    /**
     * @event betslipPlaceBet
     */
    return function (dispatch) {

        dispatch(BetslipResetStatus());

        function processBetResults(data) {

            //console.log("bet result", data);
            dispatch(UILoadingDone("bet"));
            if (data.result === "OK") {
                //add to favorites
                Config.isPartnerIntegration && (Config.isPartnerIntegration.mode.iframe || Config.isPartnerIntegration.needToLoginFromUrl) && dispatch(GetUserBalance(profile));
                data.details.events.map(event => { dispatch(FavoriteAdd("game", betSlipData.events[event.selection_id].gameId)); });

                Config.betting.resetAmountAfterBet && !betSlipData.quickBet && dispatch(BetslipSetStake(null));
                if (betSlipData.quickBet) {
                    showQuickBetNotification(data, "success", dispatch);
                } else {
                    dispatch(BetslipBetAccepted(data));
                }
                Config.betting.enableRetainSelectionAfterPlacment || dispatch(BetslipClear(true));
                //("removing status info in ", Config.betting.betAcceptedMessageTime);
                Config.betting.betAcceptedMessageTime && setTimeout(() => { dispatch(BetslipResetStatus()); }, Config.betting.betAcceptedMessageTime);
                Config.main.closeBetslipAfterBetTime && setTimeout(() => {
                    dispatch(UIClose("betslip"));
                }, Config.betting.closeBetslipAfterBetTime);
            } else {
                let generalBetResult;
                if (data.result === -1) {
                    generalBetResult = getBetSlipError(data);
                } else if (data.result === 3019) {
                    generalBetResult = t("Incompatible bet") + `(${data.result})`;
                } else {
                    if (errorCodes[data.result]) {
                        generalBetResult = t(getErrorMessageByCode(data.result + ((data.result === '1510' && Config.betting.allowManualSuperBet) ? '_sb' : '')));
                    } else {
                        generalBetResult = getBetSlipError(data);
                    }
                }

                if (betSlipData.quickBet) {
                    showQuickBetNotification(data, "error", dispatch);
                } else {
                    dispatch(BetslipBetFailed({ reason: generalBetResult }));
                }
                // setTimeout(() => { dispatch(BetslipResetStatus()); }, Config.betting.betAcceptedMessageTime);
            }
        }


        function updateUserProfileData(){



            axios.post('http://localhost/beys/api/get_userProfile.php', {
                uid: profile.id
            }, {
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            )
                .then(function (Ures) {
                    console.log(Ures);
                    
                    let userR = Ures.data.data;

                    let response = {};
                    
                   
                    if (Ures.data.success === 'true') {
                        response.data = { "profile": { '1': userR } };
                        dispatch(SwarmReceiveData(response.data, "profile"));
                        
                    }
                })
                .catch(function (error) {
                   
                    console.log(error);
                });
        }

        

        let betPromises = [];
        //dispatch(UILoading("bet")); //Hide Loader.
        dispatch(BetslipMarkBetProcessing(Object.keys(betSlipData.events)));
        var requests = createBetRequests(betSlipData, currency, freeBet);


        profile = profile;
        betSlipData = betSlipData;

        var betCounter = 0,
            haveAcceptedEvent = false,
            enablePopupAfterBet = false;

        if (!profile) {
            dispatch(UILoadingFailed("bet", ''));
            dispatch(OpenPopup("LoginForm"));
        }


        let gameids = [];
        for (let k = 0; k < requests.length; k++) {
            for (let i = 0; i < requests[k].bets.length; i++) {

                if (!gameids.includes(requests[k].bets[i].gameId)) {
                    gameids.push(Number(requests[k].bets[i].gameId));
                }

            }
        }


        gameids = _.uniq(gameids);
        console.log(gameids);


        let swarmRequestGames = {
            'source': 'betting',
            'what': {
                'competition': [],
                'region': ["name", "alias", "order", "id"],
                'game': [
                    'id', 'start_ts', 'team1_name', 'text_info', 'sport_alias', 'team2_name', 'game_number', 'game_external_id',
                    'team1_external_id', 'team2_external_id', 'type', 'info', 'is_stat_available',
                    'events_count', 'markets_count', 'extra', 'is_blocked', 'is_itf', 'exclude_ids',
                    'is_live', 'tv_type', 'video_id', 'video_id2', 'video_id3', 'video_provider', 'stats'
                ],
                'event': ['id', 'price', 'type', 'name', 'order', 'base'],
                'market': ['type', 'express_id', 'name', 'base', 'order', 'home_score', 'away_score'],
                'sport': ['id', 'name', 'alias']
            },
            'where': {
                'game': {
                    'id': {
                        '@in': gameids
                    }
                },
                'sport': {
                    'type': {
                        '@ne': 1
                    }
                }
            }
        };


        var searchRequests = [Zergling.get(swarmRequestGames)];
        Promise.all(searchRequests).then(response => {

            let socketResponse = processSearchResults(response);
            console.log(socketResponse);
            cookie.save('showBetLoader', true);
            let hasLivevent = _.filter(socketResponse, function (o) { return o.game.is_live === 1; });
            let MainEventIsLive = false;
            if (hasLivevent && hasLivevent.length > 0) {
                MainEventIsLive = true;
            }
            setTimeout(() => {

                var showBetLoader = cookie.load('showBetLoader');

                console.log(showBetLoader);

                if(showBetLoader == "true")
                {

                

                requests.map(request => {

                    // var max_amount = cookie.load('final_maxbet_amount');
                    // var max_bet_amount_json = cookie.load('max_bet_amount_json');


                    var posWin = cookie.load('posWin');
                    var oddsFormatType = cookie.load('oddsFormatType');

                    if (typeof oddsFormatType == "undefined") oddsFormatType = 'decimal';

                    console.log(oddsFormatType);

                    let ODD_TYPE_MAP = ['decimal', 'fractional', 'american', 'hongkong', 'malay', 'indo'];

                    let ODD_TYPE_MAP_INDEX = ODD_TYPE_MAP.indexOf(oddsFormatType); //this.props.oddsFormat

                    request["uid"] = profile.id
                    request["currency"] = profile.currency_name
                    request["is_live"] = MainEventIsLive;
                    request["possible_win"] = parseFloat(posWin);
                    request["payout"] = parseFloat(posWin);
                    request["accept_type_id"] = 0;
                    request["system_min_count"] = 2;
                    request["client_login"] = profile.username
                    request["barcode"] = 0;
                    request["calc_date"] = moment().unix();
                    request["date_time"] = moment().unix();
                    request["client_bonus_id"] = null;
                    request["bonus_bet_amount"] = "";
                    request["bonus"] = "";
                    request["is_super_bet"] = betSlipData.superBet;
                    request["is_bonus_money"] = false;
                    request["draw_number"] = null;
                    request["tags"] = null;
                    request["old_bet_id"] = null;
                    request["auto_cash_out_amount"] = null;
                    request["additional_amount"] = null;
                    request["total_partial_cashout_amount"] = null;
                    request["remaining_stake"] = betSlipData.stake;
                    request["tax_amount"] = 0.0;
                    request["additional_info"] = null;
                    request["has_recalculation_reason"] = false;
                    request["info_cashdesk_id"] = null;
                    request["stake_tax_amount"] = null;
                    request["odd_type"] = ODD_TYPE_MAP_INDEX + 1;
                    request["events"] = [];
                    //    var tempGameIds = [];




                    for (var i = 0; i < request.bets.length; i++) {


                        // if(betSlipData.type == 1){
                        //    let maxBetObjTemp =  _.find(max_bet_amount_json, function (o) { return parseInt(o.event_id) === parseInt(request.bets[i].eventId); });
                        //    max_amount = maxBetObjTemp.max_bet_amount;
                        //    // debugger;
                        // }



                        let SocketObj = _.find(socketResponse, function (o) { return parseInt(o.game.id) === parseInt(request.bets[i].gameId); });
                        var basx = "undefined";

                        if (request && request.bets[i] && request.bets[i].base) {
                            basx = String(request.bets[i].base);
                        }

                        var bs1 = basx.split('.');
                        var bs2 = bs1[0] + ' / ' + basx;
                        var bs3 = '';
                        var basx1;

                        if (basx !== "undefined") {
                            bs3 = bs2;

                        }


                        if (request && request.bets[i] && request.bets[i].base) {
                            basx1 = request.bets[i].pick + "(" + basx + ")"
                        } else {
                            basx1 = request && request.bets[i] && request.bets[i].pick ? request.bets[i].pick : '';
                        }

                        request["events"].push({
                            "selection_id": request && request.bets[i] && request.bets[i].eventId ? request.bets[i].eventId : '', //1239582239,
                            "coeficient": request.bets[i].price,
                            "outcome": 0,
                            "outcome_name": "NotResulted",
                            "game_info": request.bets[i].eventType1,
                            "event_name": request.bets[i].eventName,
                            "game_start_date": SocketObj.game.start_ts,
                            "team1": SocketObj.game.team1_name,
                            "team2": SocketObj.game.team2_name,
                            "competition_name": request.bets[i].pick,
                            "game_id": request.bets[i].gameId,
                            "is_live": SocketObj.game.is_live,

                            "sport_id": SocketObj.sport.id,
                            "sport_name": SocketObj.sport.name,
                            "sport_index": SocketObj.sport.id,

                            "region_name": SocketObj.region.name,
                            "market_name": request.bets[i].marketName,
                            "match_display_id": 15952435,
                            "game_name": request.bets[i].title,
                            "basis": bs3,
                            "maxBet": request.bets[i].maxbet_amount,
                            "match_info": createAdditionalTextInfo(SocketObj.game),
                            "home_score": SocketObj.game.info.score1,
                            "away_score": SocketObj.game.info.score2,
                            "cash_out_price": null,
                            "selection_price": request && request.bets[i] && request.bets[i].price ? request.bets[i].price : 0,
                            "exact_name": basx1,

                        });
                        //  }


                    }


                    console.log('request : ', request);



                    // betPromises.push(
                    //     Zergling.get(request, 'do_bet')
                    //         .then(processBetResults)
                    //         .catch((reason) => {
                    //             dispatch(UILoadingFailed("bet", reason));
                    //             console.log("bet failed", reason);
                    //             if (reason.code === 12) { // not logged in
                    //                 dispatch(OpenPopup("LoginForm"));
                    //             } else {
                    //                 processBetResults(reason);
                    //             }
                    //         })
                    //         .then((data) => {

                    //         })
                    // );



                    return axios.post('http://localhost/beys/api/do_bet.php', request, {
                        headers: {
                            'Content-Type': 'application/json',
                        }
                    }
                    )
                        .then(function (res) {

                            dispatch(UILoadingDone("bet"));
                            cookie.save('maxBetMsg', '')

                            if (res['data']['success'] == "OK") {


                                //  let response = { "code": 0, "rid": "157874159025163", "data": { "result": "OK", "result_text": null, "details": { "bet_id": 788759205, "k": 2.75, "amount": 1.0, "type": 1, "is_superbet": false, "FreeBetAmount": 0.0, "bonus_amount": 0.0, "IsLive": true, "events": [{ "selection_id": 1239582239, "game_id": 15952435, "k": 2.75, "m_arjel_code": "1N2MA", "competition": "Club Friendlies", "home_team": "Nyiregyhaza Spartacus", "away_team": "Debreceni VSC", "start_date": 1578736800, "market_name": "Match Result", "event_name": "W2", "IsLive": true }] } } }
                                res['data']['details']['id'] = parseInt(res['data']['details']['id']);
                                res['data']['details']['amount'] = parseFloat(res['data']['details']['amount']);
                                res['data']['details']['bonus_amount'] = parseFloat(res['data']['details']['bonus_bet_amount']);
                                res['data']['details']['k'] = parseFloat(res['data']['details']['k']);
                                res['data']['details']['type'] = parseInt(res['data']['details']['type']);

                                for (let i = 0; i < res['data']['details']['events'].length; i++) {

                                    res['data']['details']['events'][i]['game_id'] = parseInt(res['data']['details']['events'][i]['game_id'])
                                    res['data']['details']['events'][i]['selection_id'] = parseInt(res['data']['details']['events'][i]['selection_id'])
                                    res['data']['details']['events'][i]['start_date'] = parseInt(res['data']['details']['events'][i]['start_date'])
                                }

                                processBetResults(res['data'])

                                betCounter++;
                                // if (haveAcceptedEvent && requests.length === betCounter) {
                                //     // if (Config.partner.balanceRefreshPeriod || Config.main.rfid.balanceRefreshPeriod) { // refresh balance right after doing bet in integration skin
                                //     //     $rootScope.$broadcast('refreshBalance');
                                //     // }
                                //     $rootScope.$broadcast('refreshBalance');
                                //     enablePopupAfterBet && $rootScope.$broadcast('showPopupBySlug', 'after_bet');
                                // }
                                
                               
                                updateUserProfileData();
                               
                                
                                //alert("Bet Placed Sucessfully...");

                            }
                            else {

                            }
                        })
                        .catch(function (reason) {
                            //console.log(reason);

                            dispatch(UILoadingFailed("bet", reason));
                            console.log("bet failed", reason);
                            if (reason.code === 12) { // not logged in
                                dispatch(OpenPopup("LoginForm"));
                            } else {
                                processBetResults(reason);
                            }

                        });
                });

                Promise.all(betPromises).then(() => {
                    dispatch(UILoadingDone("bet"));
                });
            }
            else{
                dispatch(UILoadingDone("bet"));
            }

            }, 4000);

        });











    };
};

/**
 * @name BetslipBookBet
 * @description Book a bet
 * @param {Object} betSlipData betslip data from store
 * @returns {Function} async action dispatcher
 * @constructor
 */
export const BetslipBookBet = (betSlipData) => {
    /**
     * @event betslipGetEventMaxBet
     */

    let betEvents = betSlipData.events;
    return function (dispatch) {
        dispatch(UILoading("getBookingBetId"));
        let request = { type: betSlipData.type, amount: parseFloat(betSlipData.unitStake) };
        if (betSlipData.type === BETSLIP_TYPE_SYSTEM) {
            request.sys_bet = betSlipData.sysOptionValue;
        }
        request.bets = Object.keys(betEvents).reduce((acc, key) => acc.concat({ event_id: betEvents[key].eventId, amount: betSlipData.unitStake }), []);
        Zergling.get(request, 'book_bet')
            .then(result => {
                dispatch(UILoadingDone("getBookingBetId"));
                if (result.result === 0 && result.details && result.details.number) {
                    dispatch(OpenPopup("message", { title: t("Booking Bet"), type: "info", body: t("Your booking number is: ") + result.details.number }));
                }

            })
            .catch((reason) => {
                dispatch(UILoadingFailed("getBookingBetId", reason));
            });
    };
};