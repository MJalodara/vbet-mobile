import Zergling from '../helpers/zergling';
import { SwarmLoadingStart, SwarmLoadingDone, SwarmReceiveData } from './swarm';
import {mainTransactionTypes} from "../constants/balanceHistory";

const axios = require('axios');
import cookie from 'react-cookie';

/**
 * @name LoadBalanceHistory
 * @description  Load balance history for given product and date range;
 * @param {String} product name "Sport, Casino ..."
 * @param {Object} dateRange
 * @param {Number} type
 * @returns {Function} async action dispatcher
 */

export function LoadBalanceHistory(product, dateRange, type) {
    var swarmDataKey = "balanceHistory";

    let timeZoneOffset = new Date().getTimezoneOffset() / 60,
        where = { time_shift: -timeZoneOffset },
        balanceType = parseInt(type, 10);
    let time_shift = -timeZoneOffset;

    where.from_date = dateRange.fromDate.unix();
    where.to_date = dateRange.toDate.unix();

    let fromDate = dateRange.fromDate.unix();
    let toDate = dateRange.toDate.unix();

    if (balanceType !== -1) { // -1 means "all"
        where.type = balanceType;
    }
    let request = { where, product: product || undefined };

    return function (dispatch) {
        dispatch(SwarmLoadingStart(swarmDataKey));
        // Zergling
        //     .get(request, "balance_history")
        //     .then(response => {
        //         dispatch(SwarmReceiveData(response, swarmDataKey));
        //     })
        //     .catch()
        //     .then(() => dispatch(SwarmLoadingDone(swarmDataKey)));
        let userId = cookie.load('authIndex');
        axios.post('http://localhost/beys/api/get_balance.php', {
            'uid': userId, 'time_shift': time_shift, 'from_date': fromDate, 'to_date': toDate
        }, {
            headers: {
                'Content-Type': 'application/json',
            }
        }
        )
            .then(function (response) {
                let history = [];
                history = response.data && response.data.history ? response.data.history : [];
                
                if (history.length > 0) {
                    for (i = 0, length = history.length; i < length; i += 1) {
                        if (!!parseFloat(history[i].balance_after_win)) {
                            history[i].amount = "+" + history[i].possible_win;
                            history[i].bonus = parseFloat(history[i].bonus);
                            history[i].bet_id = parseFloat(history[i].id);
                            history[i].transaction_id = "";
                            history[i].operation = '1';
                            history[i].operation_name = "Bet";
                            history[i].balance = parseFloat(history[i].balance_after_win); // get current balanace
                            history[i].date_time = parseFloat(history[i].date_time);
                            history[i].game = "SportsBook";
                            history[i].product_category = 1;
                            history[i].product = "Betting";
                            history[i].payment_system_id; null;
                            history[i].payment_system_name = null;
                            history[i].buddy_id = null;
                            history[i].buddy_name = null;
                            history[i].name = "";

                            if (history[i].payment_system_name) {
                                history[i].name += ' (' + history[i].payment_system_name + ')';
                            }
                        }

                        if (!parseFloat(history[i].balance_after_win)) {
                            history[i].amount = "-" + history[i].amount;
                            history[i].bonus = parseFloat(history[i].bonus);
                            history[i].bet_id = parseFloat(history[i].id);
                            history[i].transaction_id = "";
                            history[i].operation = '0';
                            history[i].operation_name = "Bet";
                            history[i].balance = parseFloat(history[i].balanace_after); // get current balanace
                            history[i].date_time = parseFloat(history[i].date_time);
                            history[i].game = "SportsBook";
                            history[i].product_category = 1;
                            history[i].product = "Betting";
                            history[i].payment_system_id; null;
                            history[i].payment_system_name = null;
                            history[i].buddy_id = null;
                            history[i].buddy_name = null;
                            history[i].name = "";
                            if (history[i].payment_system_name) {
                                history[i].name += ' (' + history[i].payment_system_name + ')';
                            }
                        }
                    }
                }
                if (response.data.success == "true") {
                    let data = {};
                    data.history = history;
                    dispatch(SwarmReceiveData(data, swarmDataKey));
                } else {
                    console.log('failed loading balance history');
                }

                
                dispatch(SwarmLoadingDone(swarmDataKey))
            })
            .catch(function (error) {
                dispatch(SwarmLoadingDone(swarmDataKey))
            });
    };
}
