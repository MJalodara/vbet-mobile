import Zergling from '../helpers/zergling';
import Helpers from "../helpers/helperFunctions";
import { UserProfileUpdateReceived } from "../actions/user";
const axios = require('axios');
import cookie from 'react-cookie';

export function GetUserBalance(profile) {
    let newProfile = Helpers.cloneDeep(profile);
    return function (dispatch) {
        Zergling.get({}, 'get_balance')
            .then((data) => {
                let userId = Object.keys(data.data.profile)[0];
                newProfile.balance = data.data.profile[userId].balance;
                dispatch(UserProfileUpdateReceived(newProfile));
            })
            .catch();
    };
}


async function funGetGameResult(bets) {
    let gameids = [];
    for (let i = 0; i < bets.length; i++) {

        for (let j = 0; j < bets[i].events.length; j++) {

            if (!gameids.includes(bets[i].events[j].game_id)) {
                gameids.push(bets[i].events[j].game_id);
            }

        }
    }


    let gameObject = [];

    for (let v = 0; v < bets.length; v++) {
        for (let j = 0; j < bets[v].events.length; j++) {
            for (let m = 0; m < gameids.length; m++) {
                if (gameids[m] == bets[v].events[j].game_id) {
                    gameObject.push(bets[v].events[j]);
                }
            }
        }
    }

    for (let i = 0; i < gameObject.length; i++) {
        let waitValue = await new Promise((success, reject) => { setTimeout(() => success(2), 15000) })
        getResultDetails(gameObject[i]);
    }
}

function getResultDetails(game) {
    var gameId = parseInt(game.game_id);
    var sportId = parseInt(game.sport_id);

    Zergling.get({ 'game_id': gameId }, 'get_results').then(function (result) {

        if (result.lines && result.lines.line) {
            let newObj = [];
            let arr = result.lines.line;
            for (let i = 0; i < arr.length; i++) {

                let flag = false;
                let find_index = 0;
                for (let j = 0; j < newObj.length; j++) {
                    if (newObj[j]['line_name'] === arr[i].line_name) //replace(/ /g, '').replace(/[^a-zA-Z ]/g, "")) //replace(/\s+/g, '_')) {
                    {
                        flag = true;
                        find_index = j;
                    }
                }

                if (flag) {
                    let oldValue = newObj[find_index].value.replace('"', " ");
                    newObj[find_index]['value'] = oldValue + "," + arr[i].events.event_name.join(",").replace('"', " ")
                } else {

                    newObj.push({
                        'line_name': arr[i].line_name.replace('"', " "), //replace(/ /g, '').replace(/[^a-zA-Z ]/g, ""), //replace(/\s+/g, '_'),
                        'value': arr[i].events.event_name.join(",").replace('"', " ")
                    })
                }
            }

            saveGameResult(gameId, sportId, newObj);
        }

    })['finally'](function (reason) {
        // game.additionalDetailsAreLoading = false;
    });
    // }
}

function saveGameResult(game_id, sportId, newObj) {
    let userId = cookie.load('authIndex');
    axios.post('http://localhost/beys/api/save_result.php', {
        'uid': userId, 'game_id': game_id, 'sport_id': sportId, 'tableData': newObj
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    )
        .then(function (response) {
            if (response.data.success == "true") {
            } else {
                console.log('failed loading my bets history');
            }

        })
        .catch(function (error) {
        });

}



export function GetMyBetDetails() {
    self = this;
    let userId = cookie.load('authIndex');
    axios.post('http://localhost/beys/api/get_mybets.php', {
        'uid': userId
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    )
        .then(function (response) {
            if (response.data.success == "true") {
                var response = response.data.data;

                if (response.bets.length > 0) {
                    funGetGameResult(response.bets);
                } //getResultDetails(response.bets[0]['events'][0]);
            } else {
                console.log('failed loading my bets history');
            }

        })
        .catch(function (error) {
            console.log(error);
        });

}

export function getMyProfile() {
    self = this;
    let userId = cookie.load('authIndex');
    axios.post('http://localhost/beys/api/get_userProfile.php', {
        uid: userId
    }, {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    )
        .then(function (response) {
            if (response.data.success === 'true') {
                return response.data.data;
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}


export function getMatchInfo(event) {
    self = this;
     return new Promise((resolve, reject) => {
    Zergling.get({ "match_id_list": [event.game_id] }, 'get_match_scores')
        .then((data) => {
            resolve(data.details[0].score);
        });
    });
  }