import Zergling from '../helpers/zergling';
import {SwarmLoadingStart, SwarmLoadingDone, SwarmReceiveData, SwarmClearData} from './swarm';
import {SET_BET_HISTORY_FILTERS, RESET_BET_HISTORY_FILTERS, DESTROY_LOADED_BET_HISTORY, SET_CASH_OUT_VALUE} from "../actions/actionTypes/";

const axios = require('axios');
import cookie from 'react-cookie';

export function UpdateHistoryBets (bets, betTypes, type) {
    /**
     * @event UpdateHistoryBets
     */
    return {
        type,
        payload: {
            bets
        },
        meta: {
            betTypes
        }
    };
}

/**
 * @name LoadBetHistory
 * @description  Load Bets history for given product and date range;
 * @param {Boolean} unsettled
 * @param {Object} options
 * @returns {Function} async action dispatcher
 */
export function LoadBetHistory (unsettled = false, options = {}) {
    let swarmDataKey = "betHistory" + (unsettled ? "_unsettled" : "_settled"),
        {from_date, to_date, time_shift, product} = options,
        request = {
            where: {
                with_pool_bets: true,
                from_date,
                to_date,
                time_shift
            },
            product
        };
    if (unsettled) {
        request.where.outcome = 0;
    }

    let iSopenBet = unsettled;
    let req_options = options;
    return function (dispatch) {
        dispatch(SwarmLoadingStart(swarmDataKey));
        // Zergling
        //     .get(request, "bet_history")
        //     .then(response => {
        //         dispatch(SwarmReceiveData(response, swarmDataKey));
        //     })
        //     .catch()
        //     .then(() => dispatch(SwarmLoadingDone(swarmDataKey)));
        
        if(req_options.range == "0"){
            var start = new Date();
            start.setHours(0,0,0,0);

            var end = new Date();
            end.setHours(23,59,59,999);
            
            req_options.from_date = start.getTime()/1000;
            req_options.to_date = end.getTime()/1000;
        }


        let userId = cookie.load('authIndex');
        axios.post('http://localhost/beys/api/get_mybets.php', {
            'uid': userId,
            'from_date':req_options.from_date,
            'to_date':req_options.to_date,
            'range':req_options.range,
            'iSopenBet':iSopenBet
        }, {
            headers: {
                'Content-Type': 'application/json',
            }
        }
        )
            .then(function (res) {
                

                if (res.data.success == "true") {

                var response = res.data.data;


                for (var i = 0; i < response.bets.length; i++) {
                    response.bets[i]['odd_type'] = parseInt(response.bets[i]['odd_type']);
                    response.bets[i]['amount'] = parseFloat(response.bets[i]['amount']);
                    response.bets[i]['k'] = parseFloat(response.bets[i]['k']);
                    response.bets[i]['outcome'] = parseInt(response.bets[i]['outcome']);
                    response.bets[i]['client_id'] = parseInt(response.bets[i]['client_id']);
                    response.bets[i]['is_live'] = parseInt(response.bets[i]['is_live']);
                    response.bets[i]['payout'] = parseInt(response.bets[i]['payout']);
                    response.bets[i]['possible_win'] = parseInt(response.bets[i]['possible_win']);
                    response.bets[i]['accept_type_id'] = parseInt(response.bets[i]['accept_type_id']);
                    response.bets[i]['barcode'] = parseInt(response.bets[i]['barcode']);
                    response.bets[i]['calc_date'] = parseInt(response.bets[i]['calc_date']);
                    response.bets[i]['date_time'] = parseInt(response.bets[i]['date_time']);
                    response.bets[i]['bonus_bet_amount'] = parseFloat(response.bets[i]['bonus_bet_amount']);
                    response.bets[i]['bonus'] = parseFloat(response.bets[i]['bonus']);
                    response.bets[i]['source'] = parseInt(response.bets[i]['source']);
                    response.bets[i]['old_bet_id'] = parseInt(response.bets[i]['old_bet_id']);
                    response.bets[i]['remaining_stake'] = parseFloat(response.bets[i]['remaining_stake']);
                    response.bets[i]['tax_amount'] = parseFloat(response.bets[i]['tax_amount']);
                    response.bets[i]['FreeBetAmount'] = parseInt(response.bets[i]['FreeBetAmount']);
                    for (var j = 0; j < response.bets[i]['events'].length; j++) {
                        response.bets[i]['events'][j]['selection_id'] = parseInt(response.bets[i]['events'][j]['selection_id']);
                        response.bets[i]['events'][j]['game_id'] = parseInt(response.bets[i]['events'][j]['game_id']);
                        response.bets[i]['events'][j]['start_date'] = parseInt(response.bets[i]['events'][j]['start_date']);

                        if (response.bets[i]['events'][j]['basis']) {

                            response.bets[i]['events'][j]['event_name'] = response.bets[i]['events'][j]['competition_name'] + ' (' + response.bets[i]['events'][j]['basis'].replace("/", " / ") + ')';
                        } else {
                            response.bets[i]['events'][j]['event_name'] = response.bets[i]['events'][j]['competition_name']
                        }

                    }
                }


                   
                    dispatch(SwarmReceiveData(response, swarmDataKey));
                } else {
                    console.log('failed loading my bets history');
                }

                
                dispatch(SwarmLoadingDone(swarmDataKey))
            })
            .catch(function (error) {
                dispatch(SwarmLoadingDone(swarmDataKey))
            });
    };
}

/**
 * @name SetBetHistoryFilters
 * @description Append Bet History filters data;
 * @param {Object} payload
 * @returns {Object}
 */
export function SetBetHistoryFilters (payload) {
    return {
        type: SET_BET_HISTORY_FILTERS,
        payload
    };
}

/**
 * @name SetBetHistoryFilters
 * @description Append Bet History filters data;
 * @param {Object} payload
 * @returns {Object}
 */
export function SetCashOutValue (payload) {
    return {
        type: SET_CASH_OUT_VALUE,
        payload
    };
}

/**
 * @name ResetBetHistoryFiltersToDefault
 * @description Reset bet history filters to default
 * @returns {Object}
 */
export function ResetBetHistoryFiltersToDefault () {
    return {
        type: RESET_BET_HISTORY_FILTERS
    };
}

/**
 * @name ResetLoadedBetHistory
 * @description Reset loaded bet history filters
 * @returns {Object}
 */
export function ResetLoadedBetHistory () {
    return {
        type: DESTROY_LOADED_BET_HISTORY
    };
}

/**
 * @name DoCashOut
 * @description Do cash out for given betId and price
 * @param {Number} betId
 * @param {Number} price
 * @param {Number} partialPrice
 * @returns {Function} async action dispatcher
 */
export function DoCashOut (betId, price, partialPrice = null) {
    var swarmDataKey = "cashOut";

    return function (dispatch) {
        dispatch(SwarmClearData(swarmDataKey));
        dispatch(SwarmLoadingStart(swarmDataKey));
        Zergling
            .get({bet_id: betId, price, partial_price: partialPrice}, 'cashout')
            .then(response => {
                dispatch(SwarmReceiveData(response, swarmDataKey));
            })
            .catch()
            .then(() => dispatch(SwarmLoadingDone(swarmDataKey)));
    };
}
